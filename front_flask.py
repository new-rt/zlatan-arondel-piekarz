from flask import Flask, render_template, request, redirect, url_for, flash, session
import os, waitress, requests, json, sqlite3

app = Flask(__name__)

#Créez 4 endpoints basés sur base.j2 pour consulter la bibliothèque : accueil, emprunts, livres et resultats
@app.route('/')
def accueil():
    return render_template('base.j2')

@app.route('/emprunts')
def emprunts():
    return render_template('base.j2')

@app.route('/livres')
def livres():
    return render_template('base.j2')

@app.route('/resultats')
def resultats():
    return render_template('base.j2')


api_service_url = os.environ.get("API_SERVICE_URL", "http://api:5007")
URL_EXO = api_service_url + "/"

waitress.serve(app, host='0.0.0.0', port=5000)

