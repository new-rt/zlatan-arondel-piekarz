#fastAPI 

from fastapi import FastAPI, HTTPException, Path, Query, Body
from pydantic import BaseModel
import sqlite3
from datetime import datetime
import uvicorn, nest_asyncio

app = FastAPI()

# Fonction pour se connecter à la base de données
def connect_db():
    return sqlite3.connect('ma_base_de_donnees.db')

# Modèle Pydantic pour la création d'un livre
class LivreCreate(BaseModel):
    titre: str
    pitch: str
    auteur: str
    date_public: str

# Modèle Pydantic pour la création d'un utilisateur
class UtilisateurCreate(BaseModel):
    nom: str
    email: str

# Endpoint pour obtenir la liste complète des utilisateurs
@app.get('/utilisateurs', response_model=list)
async def get_utilisateurs():
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM utilisateurs')
    utilisateurs = cursor.fetchall()
    conn.close()
    return utilisateurs

# Endpoint pour obtenir la liste complète des livres
@app.get('/livres', response_model=list)
async def get_livres():
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM livres')
    livres = cursor.fetchall()
    conn.close()
    return livres

# Endpoint pour obtenir la liste complète des auteurs
@app.get('/auteurs', response_model=list)
async def get_auteurs():
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM auteurs')
    auteurs = cursor.fetchall()
    conn.close()
    return auteurs

# Endpoint pour obtenir les détails d'un utilisateur par ID ou nom
@app.get('/utilisateur/{utilisateur}', response_model=dict)
async def get_utilisateur(utilisateur: str = Path(..., description="ID ou nom de l'utilisateur")):
    conn = connect_db()
    cursor = conn.cursor()

    try:
        utilisateur_id = int(utilisateur)
        cursor.execute('SELECT * FROM utilisateurs WHERE id = ?', (utilisateur_id,))
    except ValueError:
        cursor.execute('SELECT * FROM utilisateurs WHERE nom_utilisateur = ?', (utilisateur,))

    result = cursor.fetchall()

    if not result:
        raise HTTPException(status_code=404, detail="Utilisateur non trouvé")
    elif len(result) > 1:
        raise HTTPException(status_code=400, detail="Plusieurs utilisateurs avec le même nom")

    conn.close()
    return result[0]

# Endpoint pour obtenir les livres empruntés par un utilisateur par ID ou nom
@app.get('/utilisateur/emprunts/{utilisateur}', response_model=list)
async def get_emprunts_utilisateur(utilisateur: str = Path(..., description="ID ou nom de l'utilisateur")):
    conn = connect_db()
    cursor = conn.cursor()

    try:
        utilisateur_id = int(utilisateur)
        cursor.execute('SELECT * FROM livres WHERE emprunteur_id = ?', (utilisateur_id,))
    except ValueError:
        cursor.execute('SELECT * FROM utilisateurs WHERE nom_utilisateur = ?', (utilisateur,))
        result = cursor.fetchall()

        if not result or len(result) > 1:
            raise HTTPException(status_code=404, detail="Utilisateur non trouvé ou plusieurs utilisateurs avec le même nom")

        cursor.execute('SELECT * FROM livres WHERE emprunteur_id = ?', (result[0][0],))

    emprunts = cursor.fetchall()
    conn.close()
    return emprunts

# Endpoint pour obtenir les livres d'un siècle spécifique
@app.get('/livres/siecle/{numero}', response_model=list)
async def get_livres_siecle(numero: str = Path(..., description="Numéro du siècle")):
    conn = connect_db()
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM livres WHERE strftime("%Y", date_public) LIKE ?', (f'{numero}%',))
    livres = cursor.fetchall()
    conn.close()
    return livres

# Endpoint pour ajouter un livre
@app.post('/livres/ajouter', response_model=dict)
async def ajouter_livre(livre: LivreCreate):
    conn = connect_db()
    cursor = conn.cursor()

    # Vérifier si l'auteur existe
    cursor.execute('SELECT id FROM auteurs WHERE nom_auteur = ?', (livre.auteur,))
    auteur_result = cursor.fetchone()

    if not auteur_result:
        # Ajouter l'auteur s'il n'existe pas
        cursor.execute('INSERT INTO auteurs (nom_auteur) VALUES (?)', (livre.auteur,))
        auteur_id = cursor.lastrowid
    else:
        auteur_id = auteur_result[0]

    # Ajouter le livre
    cursor.execute('INSERT INTO livres (titre, pitch, auteur_id, date_public) VALUES (?, ?, ?, ?)',
                   (livre.titre, livre.pitch, auteur_id, livre.date_public))

    conn.commit()
    conn.close()

    return {'message': 'Livre ajouté avec succès'}

# Endpoint pour ajouter un utilisateur
@app.post('/utilisateur/ajouter', response_model=dict)
async def ajouter_utilisateur(utilisateur: UtilisateurCreate):
    conn = connect_db()
    cursor = conn.cursor()

    # Vérifier si l'utilisateur existe
    cursor.execute('SELECT id FROM utilisateurs WHERE nom_utilisateur = ?', (utilisateur.nom,))
    utilisateur_result = cursor.fetchone()

    if not utilisateur_result:
        # Ajouter l'utilisateur s'il n'existe pas
        cursor.execute('INSERT INTO utilisateurs (nom_utilisateur, email_utilisateur) VALUES (?, ?)',
                       (utilisateur.nom, utilisateur.email))

        conn.commit()
        conn.close()

        return {'message': 'Utilisateur ajouté avec succès'}
    else:
        conn.close()
        return {'message': 'Utilisateur existant'}

# Endpoint pour supprimer un utilisateur par ID
@app.delete('/utilisateur/{utilisateur_id}/supprimer', response_model=dict)
async def supprimer_utilisateur(utilisateur_id: int = Path(..., description="ID de l'utilisateur")):
    conn = connect_db()
    cursor = conn.cursor()

    cursor.execute('DELETE FROM utilisateurs WHERE id = ?', (utilisateur_id,))

    conn.commit()
    conn.close()

    return {'message': 'Utilisateur supprimé avec succès'}

# Endpoint pour emprunter un livre
@app.put('/utilisateur/{utilisateur_id}/emprunter/{livre_id}', response_model=dict)
async def emprunter_livre(utilisateur_id: int, livre_id: int):
    conn = connect_db()
    cursor = conn.cursor()

    cursor.execute('UPDATE livres SET emprunteur_id = ? WHERE id = ?', (utilisateur_id, livre_id))

    conn.commit()
    conn.close()

    return {'message': 'Livre emprunté avec succès'}

# Endpoint pour rendre un livre
@app.put('/utilisateur/{utilisateur_id}/rendre/{livre_id}', response_model=dict)
async def rendre_livre(utilisateur_id: int, livre_id: int):
    conn = connect_db()
    cursor = conn.cursor()

    cursor.execute('UPDATE livres SET emprunteur_id = NULL WHERE id = ? AND emprunteur_id = ?', (livre_id, utilisateur_id))

    conn.commit()
    conn.close()

    return {'message': 'Livre rendu avec succès'}


nest_asyncio.apply()
uvicorn.run(app, host='0.0.0.0', port=5009)