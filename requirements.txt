fastapi==0.104.1
nest_asyncio==1.5.8
pydantic==2.5.1
uvicorn==0.24.0.post1
Werkzeug==2.3.7

